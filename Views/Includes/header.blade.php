<div class="row showLandscape">
	<header class="col-12">
		<!-- <p class="iconMenu" style="color: black; position: absolute; z-index: 10; bottom: 0"><i class="fa fa-bars"></i></p> -->

		<div class="row rowMenu">
			<div class="fondoBlancoHeader"></div>
			<div class="botonesMenuDesktop menuLateral" style="position: absolute; top: 48%; padding-left: 1%; z-index: 16; cursor: pointer;">
				<img src="{{asset('img/header/btnFndAzul2.png')}}" class="img-fluid" style="display: inline-block;">
				<p class="arrb fuenteNaranja" style="display: inline-block;">MENU</p>
			</div>
			<div class="marcoAzul" style="">
				
			</div>
			<div class="col-4">
				<div class="row">
					<div class="col-4 paddingFix">
						<a href="{{route('Adopciones')}}"><img class="img-fluid" src="{{asset('img/header/btnFndAzul2.png')}}"><p class="fuenteNaranja"><strong>ADOPTAR</strong></p></a>
					</div>
					<div class="col-4 paddingFix">
						<a href="{{route('buscados')}}"><img class="img-fluid" src="{{asset('img/header/btnFndAzul2.png')}}"><p class="fuenteNaranja"><strong>BUSCADOS</strong></p></a>
					</div>
					<div class="col-4 paddingFix">
						<a href="{{route('rescate')}}"><img class="img-fluid" src="{{asset('img/header/btnFndAzul2.png')}}"><p class="fuenteNaranja"><strong>RESCATE</strong></p></a>
					</div>
				</div>
			</div>

			<div class="col-4">
				<div class="row">
					<div class="col-10 offset-1">		
						
						<a href="{{route('home')}}"><img class="img-fluid" src="{{asset('img/header/logo2.png')}}"></a>
					</div>
				</div>
				
			</div>
			
			<div class="col-4">
				<div class="row">
					<div class="col-4 paddingFix">
						<a href="{{route('mision')}}"><img class="img-fluid" src="{{asset('img/header/btnFndNaranja2.png')}}"><p class="fuenteAzul"><strong>MISIÓN</strong></p></a>
					</div>

					<?php

					if (Auth::user() == null)
						{  ?>
							<div class="col-4 paddingFix">
								<a href="{{ route('login') }}"><img class="img-fluid" src="{{asset('img/header/btnFndNaranja2.png')}}"><p class="fuenteAzul"><strong>INICIAR SESION</strong></p></a>
							</div>
							<?php }
							else
							{
								?>
								<div class="col-4 paddingFix">
									<a href="{{ route('logout') }}"  
									onclick="event.preventDefault();
									document.getElementById('logout-form').submit();"><img class="img-fluid" src="{{asset('img/header/btnFndNaranja2.png')}}"><p class="fuenteAzul"><strong>SALIR</strong></p></a>
								</div>
								<?php 	} ?>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
								<div class="col-4 paddingFix">
									<a href="{{route('contacto')}}"><img class="img-fluid" src="{{asset('img/header/btnFndNaranja2.png')}}"><p class="fuenteAzul"><strong>CONTACTO</strong></p></a>
								</div>
							</div>
						</div>
			<!-- <a href="/logout" 
				onclick="event.preventDefault()
						 document.getElementById('logout-form').submit();">LOGOUT</a>
						 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
            <a href="{{ route('login') }}">Login</a>
            <a href="{{ route('register') }}">Register</a> -->

        </div>
    </header>
</div>
<div class="row" style="">
	<header class="showMobile col-12 imgRelative"">
		<div class="row imgAbsolute">
			<div class="col-12 botonesMenuMobile menuLateral" style="z-index: 16; cursor: pointer;">
				<img src="{{asset('img/header/btnFndAzul2.png')}}" class="img-fluid" style="display: inline-block;">
				<p class="arrb fuenteNaranja" style="display: inline-block;">MENU</p>
			</div>
		</div>
		<div class="row imgAbsolute redesSocialesMobile" style="right: 0">
			<div class="col-12 ">
				<img class="img-fluid" width="20" height="20" src="{{asset('img/header/fb.png')}}">
				<img class="img-fluid" width="20" height="20" src="{{asset('img/header/twitter.png')}}">
				<img class="img-fluid" width="20" height="20" src="{{asset('img/header/instagram.png')}}">
				<img class="img-fluid" width="20" height="20" src="{{asset('img/header/youtube.png')}}">
			</div>
		</div>
		<div class="row">
			<div class="fondoBlancoHeader" style="height: 40%;"></div>
			<div class="marcoAzulMobile">
			</div>
			<div class="col-10 offset-1 col-sm-6 offset-sm-3 col-md-8 offset-md-2">
				<img class="img-fluid" src="{{asset('img/header/logo2.png')}}">
			</div>
		</div>
		
	</header>
</div>


