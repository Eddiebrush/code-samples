	<footer class="row footer showDesktopFooter">
		<div class="col-3 offset-2 col-lg-3 offset-lg-2 col-xl-3 offset-xl-2">
			<div class="row">
				<div class="col-7 offset-5 col-xl-7 offset-xl-5">
					<a target="_blank" href="http://www.flacma.lat/"><img src="{{asset('img/footer/logoFlac.png')}}" class="img-fluid"></a>
				</div>
			</div>
		</div>
		<div class="col-2 offset-0 col-xl-2 offset-xl-0 centrar">
			<div class="row">
				<div class="col offset-0 col-xl-10 offset-xl-1">
					<img src="{{asset('img/footer/patrocinan.png')}}" class="img-fluid"></a>
				</div>	
			</div>
		</div>
		<div class="col-3 offset-0 col-xl-3 offset-xl-0">
			<div class="row">
				<div class="col-7 col-xl-7 offset-xl-0">	
					<a target="_blank" href="http://www.famargentina.org.ar/"><img src="{{asset('img/footer/logoFam.png')}}" class="img-fluid">
					</div>	
				</div>
			</div>
		</footer>
		<div class="row">
			<div class="col" id="container-footer" style="position: relative;">
				<footer class="row footer showMobileFooter">
					<div class="col-5 offset-1 col-sm-4 offset-sm-1 col-md-4 offset-md-1 col-lg-2 offset-lg-3 col-xl-2 offset-xl-3">
						<div class="row">
							<div class="col col-lg-9 col-xl-7 offset-xl-1">
								<a target="_blank" href="http://www.flacma.lat/"><img src="{{asset('img/footer/logoFlac.png')}}" class="img-fluid"></a>
							</div>
						</div>
					</div>
					<div class="col-5 offset-1 col-sm-4 offset-sm-2 col-md-4 offset-md-2 col-lg-2 col-xl-2">
						<div class="row">
							<div class="col col-lg-9 col-xl-7 offset-xl-1">	
								<a target="_blank" href="http://www.famargentina.org.ar/"><img src="{{asset('img/footer/logoFam.png')}}" class="img-fluid"></a>
							</div>	
						</div>
					</div>
				</footer>
			</div>
		</div>

