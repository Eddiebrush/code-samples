<header>
 <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="{{route('panelUsuarios')}}">TOMSI | Panel de Usuario</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('home')}}">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Adopciones
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ route('nuevaAdopcion') }}">Nueva adopción</a>
          <a class="dropdown-item" href="{{ route('administrar_adopciones') }}">Administrar adopciones</a>
          <!-- <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a> -->
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Buscados
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('nuevoPerroPerdido')}}">Perdí mi perro</a>
            <a class="dropdown-item" href="{{route('perrosPerdidos')}}">Administrar Perros Perdidos</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Rescates
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('nuevo_rescate')}}">Encontre un perro</a>
            <a class="dropdown-item" href="{{route('administrar_rescates')}}">Administrar Rescates</a>
          </div>

        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Municipios / O.N.G.
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('nuevo_municipio')}}">Registrarse como Municipio</a>
            <a class="dropdown-item" href="{{route('nueva_ong')}}">Registrarse como O.N.G.</a>
          </div>

        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Veterinarias
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('nueva_veterinaria')}}">Registrarse como Veterinaria</a>
            <a class="dropdown-item" href="{{route('nueva_venta_alimentos')}}">Registrarse como Alimentos/Nutrición</a>
            <a class="dropdown-item" href="{{route('nuevo_paseador')}}">Registrarse como paseador de perros</a>
          </div>

        </li>
        
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#"></a>
      </li>

    </ul>

  </div>
</nav>
<div class="divHeaderRelative" style="position: relative;">

</div>
</header>
<div>
  @if(session()->has('message'))
  @if(session()->get('estado') == 'error')
  <div class="alert alert-danger" role="alert">
    {{ session()->get('message') }}
  </div>
  @else
  <div class="alert alert-success" role="alert">
    {{ session()->get('message') }}
  </div>
  @endif
  @endif
</div>
