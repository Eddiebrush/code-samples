<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta property="og:image" content="http://tomsi.com.ar/img/rel3-4/slider_1.jpg" />
<meta property="og:title" content="Tomsi Web" />
<meta property="og:type" content="article" />
<meta property="og:description" content="¡Unite a la campaña nacional!" />
<title>Tomsi</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('css/header.css')}}">
<link rel="stylesheet" href="{{asset('css/footer.css')}}">
<link rel="stylesheet" href="{{asset('css/home.css')}}">
<link rel="stylesheet" href="{{asset('css/mision.css')}}">
<link rel="stylesheet" href="{{asset('css/menuLateral.css')}}">
<link rel="stylesheet" href="{{asset('css/adopciones.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link rel="stylesheet" href="{{asset('css/veterinarias.css')}}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script type="text/javascript" src="{{asset('js/highslide.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/highslide.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/entretenimiento.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
<script type="text/javascript">
    // override Highslide settings here
    // instead of editing the highslide.js file
    var route = "{{asset('icon/graphics/')}}"+'/'
    hs.graphicsDir = route;
</script>

<script src='https://www.google.com/recaptcha/api.js'></script>