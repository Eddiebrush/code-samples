<!DOCTYPE html>
<html lang="en">
<head>
	@include('Includes/head')
</head>
<body>
	<div class="container-fluid">
		@include('Includes/header')
		<div>
			@if(session()->has('message'))
			@if(session()->get('estado') == 'error')
			<div class="alert alert-danger" role="alert">
				{{ session()->get('message') }}
			</div>
			@else
			<div class="alert alert-success" role="alert">
				{{ session()->get('message') }}
			</div>
			@endif
			@endif
		</div>
		@yield('body')
		@include ('Includes/footer')
	</div>
	@include('Includes/scripts')
</body>
</html>