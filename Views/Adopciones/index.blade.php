@extends('Layout.default')
@section('body')

@include('Includes/menu')
<div class="container">
	
	<div class="row noData1">
		<div class="col-10 offset-1 noData2">
			<br>
			<p><h1>No hay perros en adopción en este momento tu mismo puedes ofrecer uno desde el panel de control o haciendo click <a href="{{route('nuevaAdopcion')}}">aquí</a></h1></p>
			<br>
			<br>
			<br>
		</div>
	</div>
	<div class="row">

		@foreach($adopciones as $perro)
		<?php
		$localidad = $usuarios[$perro->id_usuario-1]->localidad;
		$provincia = $provincias[$usuarios[$perro->id_usuario-1]->id_provincia-1]->descripcion;
		$ubicacion = $localidad.' | '.$provincia;
		?>
		<div class="card col-3 offset-1" style="width: 18rem;">
			<img class="card-img-top" src="{{asset($perro->foto)}}" alt="Card image cap">
			<div class="card-body">
				<h5 class="card-title">{{$perro->nombre}}</h5>
				<p class="card-text">
					Raza: {{$razas[$perro->id_raza-1]->nombre}}<br>
					Tamaño: {{$perro->tamaño}}<br>
					Sexo: {{$perro->sexo}}<br>
					Edad: {{$perro->edad}}<br>
					Ubicación: {{$ubicacion}}

				</p>
				<a href="#" data-target="#gridModal{{$perro->id}}" id="{{$perro->id}}" data-toggle="modal" class="btn btn-primary">¡Ver Mas!</a>
				
			</div>
		</div>
		<!-- Modal GRID -->
		<div class="modal fade" id="gridModal{{$perro->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Ficha de Adopción de {{$perro->nombre}}</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
							<div class="row">
								<div class="col offset-1">
									<img class="img-fluid" src="{{asset($perro->foto)}}">
								</div>
							</div>
							<div class="row">
								<div class="col">
									<br>
									<p>{{$perro->notas}}</p>
									<p>
										Raza: {{$razas[$perro->id_raza-1]->nombre}}<br>
										Tamaño: {{$perro->tamaño}}<br>
										Sexo: {{$perro->sexo}}<br>
										Edad: {{$perro->edad}}<br>
										{{$perro->castrado}}<br>
										Ubicación: {{$ubicacion}}
									</p>
								</div>
							</div>
						</div>
					</div>
					<form method="POST" action="{{route('adopciones_usuario')}}">
						{{ csrf_field() }}
						<div class="modal-footer">
							<input type="hidden" name="id" value="{{$perro->id}}">
							<button type="submit" class="btn btn-primary" onclick="alert('Se ha enviado un E-Mail con tus datos al usuario que publicó esta adopción');">¡Adoptame!</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- FIN MODAL GRID -->
		@endforeach

	</div>
</div>
<script src="{{asset('js/jquery.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		@foreach($adopciones_usuario as $value)
		@if($user_id == $value->id_usuario)
		$('#{{$value->id_adopcion}}').removeClass("btn-primary");
		$('#{{$value->id_adopcion}}').addClass("btn-success");
		$('#{{$value->id_adopcion}}').addClass("disabled");
		$('#{{$value->id_adopcion}}').text("¡Ya te anotaste!");
		$('#gridModal{{$value->id_adopcion}}').remove();
		@endif
		@endforeach

		@if ($adopciones == null) {
			$('.noData1').css('display', 'block');
		}
		@else
		{
			$('.noData1').css('display', 'none');
		}
		@endif

	});
</script>
@stop