<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Adopciones;
use App\Razas;
use App\User;
use App\Provincias;
use App\AdopcionesUsuario;
use App\Http\Requests\adopciones_usuario;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class AdopcionesController extends Controller
{
	public function __construct()
	{
		$this->middleware("auth");
	}

	public function index()
	{
		$adopciones = DB::select('select * from adopciones where estado = ?', ['aprobada']);
		$razas = Razas::all();
		$usuarios = User::all();
		$provincias = Provincias::all();
		$userId = Auth::id();
		$adopciones_usuario = AdopcionesUsuario::all();
		return view('Adopciones.index')
		->with(['adopciones' => $adopciones])
		->with(['razas' => $razas])
		->with(['usuarios' => $usuarios])
		->with(['provincias' => $provincias])
		->with(['user_id' => $userId])
		->with(['adopciones_usuario' => $adopciones_usuario]);
	}

	public function adopcion_usuario(adopciones_usuario $request)
	{
		$adopcionUsuario = new AdopcionesUsuario();
		$adopcionUsuario->id_adopcion = $request->post('id');
		$adopcionUsuario->id_usuario = Auth::user()->id;
		$adopcionUsuario->save();
		$adopcion = DB::select("select u.email 'email', a.nombre 'nombre'
			from adopciones a inner join users u
			on a.id_usuario = u.id
			where a.id = ?", [$request->post('id')]);
		$usuarioSuscripto = DB::select("select * from users where id = ?", [Auth::user()->id]);
		$this->enviarMail($adopcion, $usuarioSuscripto);
		return redirect()->route('Adopciones');
	}

	public function enviarMail($adopcion, $usuario)
	{
		$data = [
			'nombre' => $usuario[0]->nombre,
			'apellido' => $usuario[0]->apellido,
			'nombreMascota' => $adopcion[0]->nombre,
			'email' => $usuario[0]->email,
			'direccion' => $usuario[0]->domicilio,
			'localidad' => $usuario[0]->localidad,
			'barrio' => $usuario[0]->barrio,
			'telefono' => $usuario[0]->telefono
		];

		Mail::send('Mail.adopcion_encontrada',$data, function($message) use ($adopcion, $usuario){
				
				$message->from('postmaster@tomsi.com.ar', 'Tomsi Web');
				$message->to($adopcion[0]->email);	
				$message->subject("Testeando");
			
		});
	}
}
