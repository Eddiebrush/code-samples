<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Razas;
use App\Http\Requests\guardar_adopcion;
use App\Http\Requests\cambiar_estado_adopcion;
use App\Http\Requests\guardar_perro_perdido;
use App\Http\Requests\cambiar_estado_perroPerdido;
use App\Http\Requests\guardar_rescate;
use App\Http\Requests\cambiar_estado_rescates;
use App\Http\Requests\guardar_municipio;
use App\Http\Requests\guardar_ong;
use App\Http\Requests\guardar_veterinaria;
use App\Http\Requests\guardar_venta_alimentos;
use App\Http\Requests\guardar_paseador;
use App\Adopciones;
use App\Provincias;
use App\Buscados;
use App\Rescates;
use App\Municipios;
use App\Ong;
use App\Veterinarias;
use App\VentaAlimentos;
use App\Paseadores;


class panelUsuariosController extends Controller
{
	public function __construct()
	{
		$this->middleware("auth");
	}

	public function index()
	{
		return view('PanelUsuarios.index');
	}

	public function nuevaAdopcion()
	{
		$razas = Razas::all();
		return view('PanelUsuarios.Adopciones.nueva_adopcion')->with(['razas' => $razas]);
	}

	public function nuevo_municipio()
	{
		$isRegistered = DB::select("select * from usuarios_municipios where id_usuario=?", [Auth::user()->id]);
		
		if ($isRegistered == null)
		{
			$provincias = Provincias::all();
			return view('PanelUsuarios.Registros.nuevo_municipio')
			->with(['provincias' => $provincias]);
			
		}
		else
		{
			return redirect()->route('panelUsuarios')
			->with('message', 'Ya se encuentra registrado como municipio')
			->with('estado', 'error');
		}
	}

	public function nueva_ong()
	{
		$isRegistered = DB::select("select * from usuarios_ong where id_usuario = ?", [Auth::user()->id]);
		if ($isRegistered == null)
		{
			$provincias = Provincias::all();
			return view('PanelUsuarios.Registros.nueva_ong')
			->with(['provincias' => $provincias]);
		}
		else
		{
			return redirect()->route('panelUsuarios')
			->with('message', 'Ya se encuentra registrado como O.N.G.')
			->with('estado', 'error');
		}
	}

	public function nueva_veterinaria()
	{
		$isRegistered = DB::select("select * from veterinarias where id_usuario = ?", [Auth::user()->id]);
		$provincias = Provincias::all();
		if ($isRegistered == null)
		{
			return view ('panelUsuarios.Registros.nueva_veterinaria')
			->with(['provincias' => $provincias]);
		}
		else
		{
			return redirect()->route('panelUsuarios')
			->with('message', 'Ya se encuentra registrado como veterinaria')
			->with('estado', 'error');
		}
	}

	public function nueva_venta_alimentos()
	{
		$isRegistered = DB::select("select * from venta_alimentos where id_usuario = ?", [Auth::user()->id]);
		$provincias = Provincias::all();
		if ($isRegistered == null)
		{
			return view ('panelUsuarios.Registros.nueva_venta_alimentos')
			->with(['provincias' => $provincias]);
		}
		else
		{
			return redirect()->route('panelUsuarios')
			->with('message', 'Ya se encuentra registrado como un punto de venta de alimentos')
			->with('estado', 'error');
		}
	}

	public function nuevo_paseador()
	{
		$isRegistered = DB::select("select * from paseadores where id_usuario = ?", [Auth::user()->id]);
		$provincias = Provincias::all();
		if ($isRegistered == null)
		{
			return view ('PanelUsuarios.Registros.nuevo_paseador')
					->with(['provincias' => $provincias]);
		}
		else
		{
			return redirect()->route('panelUsuarios')
			->with('message', 'Ya se encuentra registrado como paseador de perros')
			->with('estado', 'error');
		}
	}

	public function guardar_paseador(guardar_paseador $request)
	{
		$paseador = new Paseadores();
		$paseador->id_usuario = Auth::user()->id;
		$paseador->nombre_paseador = $request->post('txtNombre');
		$paseador->id_provincia = $request->post('cbxProvincia');
		$paseador->localidad = $request->post('txtLocalidad');
		$paseador->telefono = $request->post('txtTelefono');
		$paseador->email = $request->post('txtEmail');
		$paseador->zona_trabajo = $request->post('txtZonaTrabajo');
		$paseador->save();

		if ($request->foto1 != null)
		{
			DB::table('paseadores')
			->where('id_usuario', $paseador->id_usuario)
			->update(['foto_1' => $this->redimensionarGuardarImagen($request, $paseador->id_usuario, 'foto1', 'img/paseadores/', 800, 800, "_1")]);

		}

		if($request->foto2 != null)
		{
			DB::table('paseadores')
			->where('id_usuario', $paseador->id_usuario)
			->update(['foto_2' => $this->redimensionarGuardarImagen($request, $paseador->id_usuario, 'foto2', 'img/paseadores/', 800, 800, "_2")]);
		}

		if($request->foto3 != null)
		{
			DB::table('paseadores')
			->where('id_usuario', $paseador->id_usuario)
			->update(['foto_3' => $this->redimensionarGuardarImagen($request, $paseador->id_usuario, 'foto3', 'img/paseadores/', 800, 800, "_3")]);
		}

		return redirect()->route('panelUsuarios')
		->with('message', 'Se ha registrado correctamente como paseador de perros')
		->with('estado', 'success');


	}

	public function guardar_venta_alimentos(guardar_venta_alimentos $request)
	{
		$venta_alimentos = new VentaAlimentos();
		$venta_alimentos->id_usuario = Auth::user()->id;
		$venta_alimentos->nombre_establecimiento = $request->post('txtNombre');
		$venta_alimentos->nombre_responsable = $request->post('txtNombreResponsable');
		$venta_alimentos->direccion = $request->post('txtDireccion');
		$venta_alimentos->id_provincia = $request->post('cbxProvincia');
		$venta_alimentos->localidad = $request->post('txtLocalidad');
		$venta_alimentos->telefono = $request->post('txtTelefono');
		$venta_alimentos->web = $request->post('txtSitioWeb');
		$venta_alimentos->email = $request->post('txtEmail');
		$venta_alimentos->descripcion_larga = $request->post('notas');
		$venta_alimentos->save();

		if ($request->foto1 != null)
		{
			DB::table('venta_alimentos')
			->where('id_usuario', $venta_alimentos->id_usuario)
			->update(['foto_1' => $this->redimensionarGuardarImagen($request, $venta_alimentos->id_usuario, 'foto1', 'img/ventaAlimentos/', 800, 800, "_1")]);

		}

		if($request->foto2 != null)
		{
			DB::table('venta_alimentos')
			->where('id_usuario', $venta_alimentos->id_usuario)
			->update(['foto_2' => $this->redimensionarGuardarImagen($request, $venta_alimentos->id_usuario, 'foto2', 'img/ventaAlimentos/', 800, 800, "_2")]);
		}

		if($request->foto3 != null)
		{
			DB::table('venta_alimentos')
			->where('id_usuario', $venta_alimentos->id_usuario)
			->update(['foto_3' => $this->redimensionarGuardarImagen($request, $venta_alimentos->id_usuario, 'foto3', 'img/ventaAlimentos/', 800, 800, "_3")]);
		}

		return redirect()->route('panelUsuarios')
		->with('message', 'Se ha registrado correctamente como punto de venta de alimentos')
		->with('estado', 'success');
	}

	public function guardar_veterinaria(guardar_veterinaria $request)
	{
		$veterinarias = new Veterinarias();
		$veterinarias->id_usuario = Auth::user()->id;
		$veterinarias->nombre_establecimiento = $request->post('txtNombre');
		$veterinarias->nombre_responsable = $request->post('txtNombreResponsable');
		$veterinarias->direccion = $request->post('txtDireccion');
		$veterinarias->id_provincia = $request->post('cbxProvincia');
		$veterinarias->localidad = $request->post('txtLocalidad');
		$veterinarias->telefono = $request->post('txtTelefono');
		$veterinarias->web = $request->post('txtSitioWeb');
		$veterinarias->email = $request->post('txtEmail');
		$veterinarias->descripcion_larga = $request->post('notas');
		$veterinarias->save();

		if ($request->foto1 != null)
		{
			DB::table('veterinarias')
			->where('id_usuario', $veterinarias->id_usuario)
			->update(['foto_1' => $this->redimensionarGuardarImagen($request, $veterinarias->id_usuario, 'foto1', 'img/veterinarias/', 800, 800, "_1")]);

		}

		if($request->foto2 != null)
		{
			DB::table('veterinarias')
			->where('id_usuario', $veterinarias->id_usuario)
			->update(['foto_2' => $this->redimensionarGuardarImagen($request, $veterinarias->id_usuario, 'foto2', 'img/veterinarias/', 800, 800, "_2")]);
		}

		if($request->foto3 != null)
		{
			DB::table('veterinarias')
			->where('id_usuario', $veterinarias->id_usuario)
			->update(['foto_3' => $this->redimensionarGuardarImagen($request, $veterinarias->id_usuario, 'foto3', 'img/veterinarias/', 800, 800, "_3")]);
		}

		return redirect()->route('panelUsuarios')
		->with('message', 'Se ha registrado correctamente como Veterinaria')
		->with('estado', 'success');


	}

	public function guardar_ong(guardar_ong $request)
	{
		$ong = new Ong();
		$ong->id_usuario = Auth::user()->id;
		$ong->nombre_ong = $request->post('txtNombre');
		$ong->contacto = $request->post('txtContacto');
		$ong->direccion = $request->post('txtDireccion');
		$ong->localidad = $request->post('txtLocalidad');
		$ong->id_provincia = $request->post('cbxProvincia');
		$ong->telefonos = $request->post('txtTelefono');
		$ong->correo = $request->post('txtEmail');
		$ong->whatsapp = $request->post('txtWhatsapp');
		$ong->sitio_web = $request->post('txtSitioWeb');
		$ong->save();

		return redirect()->route("panelUsuarios")
		->with('message', 'Se ha registrado correctamente como O.N.G.')
		->with('estado', 'success');
	}

	public function guardar_municipio(guardar_municipio $request)
	{
		$municipio = new Municipios();
		$municipio->id_usuario = Auth::user()->id;
		$municipio->id_provincia = $request->post('cbxProvincia');
		$municipio->municipio = $request->post('txtMunicipio');
		$municipio->intendente = $request->post('txtIntendente');
		$municipio->direccion = $request->post('txtDireccion');
		$municipio->telefono = $request->post('txtTelefono');
		$municipio->correo = $request->post('txtEmail');
		$municipio->contactos_prensa = $request->post('txtContactosPrensa');
		$municipio->otros_contactos = $request->post('txtOtrosContactos');
		$municipio->correo_prensa = $request->post('txtEmailPrensa');
		$municipio->whatsapp = $request->post('txtWhatsapp');
		$municipio->sitio_web = $request->post('txtSitioWeb');
		$municipio->save();

		return redirect()->route("panelUsuarios")
		->with('message', 'Se ha registrado correctamente como municipio')
		->with('estado', 'success');

	}

	public function guardarAdopcion(guardar_adopcion $request)
	{
		if ($this->validarCaptcha($request))
		{
			$adopcion = new Adopciones();
			$adopcion->nombre = $request->post('nombreAnimal');
			$adopcion->estado = 'Por aprobar';
			$adopcion->tamaño = $request->post('tamaño');
			$adopcion->edad = $request->post('edad');
			$adopcion->castrado = $request->post('castrado');
			$adopcion->notas = $request->post('notas');
			$adopcion->sexo = $request->post('sexo');
			$adopcion->id_raza = $request->post('raza');
			$adopcion->id_usuario = Auth::user()->id;
			$adopcion->save();

			DB::table('adopciones')
			->where('id_usuario', Auth::user()->id)
			->where('id', $adopcion->id)
			->update(['foto' => 
				$this->redimensionarGuardarImagen($request, $adopcion->id, 'foto', 'img/adopcion/', 400, 400, "")]);



			return redirect()->route('panelUsuarios')
			->with('message', 'La solicitud ha sido enviada a un administrador para su revisión')
			->with('estado', 'success');

		}

		else
		{

			return redirect()->route('nuevaAdopcion')
			->with('message', 'Captcha Incorrecto')
			->with('estado', 'error');
		}



	}

	public function validarCaptcha($request)
	{
		$url = "https://www.google.com/recaptcha/api/siteverify";
		$secret = "6LeZXVUUAAAAAIu5B5yNPNcm1IyGAV2Rv3ciHf_v";
		$recaptcha_response_post = $request->post('g-recaptcha-response');
		$response = file_get_contents($url."?secret=".$secret."&response=".$recaptcha_response_post);
		$robot = json_decode($response);

		if ($robot->success == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function guardar_rescate(guardar_rescate $request)
	{
		if ($this->validarCaptcha($request))
		{
			$rescates = new Rescates();
			$rescates->sexo = $request->post('sexo');
			$rescates->id_raza = $request->post('raza');
			$rescates->tamaño = $request->post('tamaño');
			$rescates->notas = $request->post('notas');
			$rescates->estado = "Por aprobar";
			$rescates->id_usuario = Auth::user()->id;
			$rescates->direccion = $request->post('direccion');
			$rescates->save();

			if ($request->foto1 != null)
			{
				DB::table('rescates')
				->where('id', $rescates->id)
				->update(['foto1' => $this->redimensionarGuardarImagen($request, $rescates->id, 'foto1', 'img/rescates/', 800, 800, "_1")]);

			}

			if($request->foto2 != null)
			{
				DB::table('rescates')
				->where('id', $rescates->id)
				->update(['foto2' => $this->redimensionarGuardarImagen($request, $rescates->id, 'foto2', 'img/rescates/', 800, 800, "_2")]);
			}

			if($request->foto3 != null)
			{
				DB::table('rescates')
				->where('id', $rescates->id)
				->update(['foto3' => $this->redimensionarGuardarImagen($request, $rescates->id, 'foto3', 'img/rescates/', 800, 800, "_3")]);
			}

			return redirect()->route('administrar_rescates')
			->with('message', 'La solicitud ha sido enviada a un administrador para su revisión')
			->with('estado', 'success');
		}
		else
		{
			return redirect()->route('nuevo_rescate')
			->with('message', 'Captcha Incorrecto')
			->with('estado', 'error');
		}

		
	}

	public function guardar_perro_perdido(guardar_perro_perdido $request)
	{
		if ($this->validarCaptcha($request))
		{
			$buscados = new Buscados();
			$buscados->nombre = $request->post('nombre');
			$buscados->sexo = $request->post('sexo');
			$buscados->id_raza = $request->post('raza');
			$buscados->tamaño = $request->post('tamaño');
			$buscados->edad = $request->post('edad');
			$buscados->notas = $request->post('notas');
			$buscados->estado = "Por aprobar";
			$buscados->id_usuario = Auth::user()->id;
			$buscados->save();


			if ($request->foto1 != null)
			{
				DB::table('buscados')
				->where('id', $buscados->id)
				->update(['foto1' => $this->redimensionarGuardarImagen($request, $buscados->id, 'foto1', 'img/buscados/', 800, 800, "_1")]);

			}

			if($request->foto2 != null)
			{
				DB::table('buscados')
				->where('id', $buscados->id)
				->update(['foto2' => $this->redimensionarGuardarImagen($request, $buscados->id, 'foto2', 'img/buscados/', 800, 800, "_2")]);
			}

			if($request->foto3 != null)
			{
				DB::table('buscados')
				->where('id', $buscados->id)
				->update(['foto3' => $this->redimensionarGuardarImagen($request, $buscados->id, 'foto3', 'img/buscados/', 800, 800, "_3")]);
			}

			return redirect()->route('perrosPerdidos')
			->with('message', 'La solicitud ha sido enviada a un administrador para su revisión')
			->with('estado', 'success');
		}
		else
		{
			return redirect()->route('nuevoPerroPerdido')
			->with('message', 'Captcha Incorrecto')
			->with('estado', 'error');
		}
	}

	public function nuevo_rescate()
	{
		$razas = Razas::all();
		return view('PanelUsuarios.Rescate.nuevo_rescate')->with(['razas' => $razas]);
	}

	public function perros_perdidos()
	{
		$razas = Razas::all();
		$buscados = DB::select('select * from buscados where id_usuario = ?', [Auth::user()->id]);
		return view("PanelUsuarios.Perdidos.index")->with(['buscados' => $buscados])->with(['razas' => $razas]);
	}

	
	public function redimensionarGuardarImagen(Request $request, $lastId, $valueSelector, $rutaGuardado, $width, $height, $sufijoOpcional)
	{
		//Resizea y guarda la imagen
		$extension = $request->file($valueSelector)->getClientOriginalExtension();
		$ruta = $rutaGuardado.$lastId.$sufijoOpcional.'.'.$extension;
		Image::make($request->file($valueSelector))
		->fit($width,$height)
		->save($ruta);

		$rutaThumb = $rutaGuardado.$lastId.$sufijoOpcional.'.thumb.'.$extension;
		Image::make($request->file($valueSelector))
		->fit(120,120)
		->save($rutaThumb);

		return $ruta;
		// //Guarda la ruta en la base de datos
		// DB::table('adopciones')
		// 	->where('id_usuario', Auth::user()->id)
		// 	->where('id', $lastId)
		// 	->update(['foto' => $ruta]);

	}

	public function administrarAdopciones()
	{
		$razas = Razas::all();
		$adopciones = DB::select('select * from adopciones where id_usuario = ?', [Auth::user()->id]);
		return view('PanelUsuarios.Adopciones.administrar_adopciones')->with(['adopciones' => $adopciones])->with(['razas' => $razas]);
	}

	public function rescates()
	{
		$razas = Razas::all();
		$rescates = DB::select('select * from rescates where id_usuario = ?', [Auth::user()->id]);
		return view ('PanelUsuarios.Rescate.index')->with(['rescates' => $rescates])->with(['razas' => $razas]);
	}

	public function borrar_rescate()
	{

	}

	public function cambiar_estado_rescates(cambiar_estado_rescates $request, $estado, $id)
	{
		if ($estado == "aprobado")
		{
			if (Auth::user()->id_tipo_usuario == 3)
				{
					DB::table('rescates')
					->where('id', $id)
					->update(['estado' => $estado]);
					return redirect()->route('administrar_rescates_admin');
				}
				else
				{
					return redirect()->route('home');
				}
			}
			else if ($estado == "borrar")
			{
				DB::table('rescates')
				->where('id', $id)
				->where('id_usuario', Auth::user()->id)
				->delete();
				return redirect()->route('administrar_rescates');
			}



		}

		public function nuevo_perdido()
		{
			$razas = Razas::all();
			$provincias = Provincias::all();
			return view ('PanelUsuarios.Perdidos.nuevo_perdido')->with(['razas' => $razas])->with(['provincias' => $provincias]);
		}

		public function cambiar_estado_perroPerdido(cambiar_estado_perroPerdido $request, $estado, $id)
		{
			if ($estado == "aprobado")
			{
				if (Auth::user()->id_tipo_usuario == 3)
					{
						DB::table('buscados')
						->where('id', $id)
						->update(['estado' => $estado]);

						return redirect()->route('administrar_buscados_admin');
					}
				}
				else if ($estado == "encontrado")
				{
					DB::table('buscados')
					->where('id', $id)
					->where('id_usuario', Auth::user()->id)
					->update(['estado' => $estado]);
				}
				else if ($estado == "borrar")
				{
					DB::table('buscados')
					->where('id', $id)
					->where('id_usuario', Auth::user()->id)
					->delete();

					if(Auth::user()->id_tipo_usuario == 3)
						{
							DB::table('buscados')
							->where('id', $id)
							->delete();
							return redirect()->route('administrar_buscados_admin');
						}
					}
					return redirect()->route('perrosPerdidos');
				}

				public function cambiarEstadoAdopcion(cambiar_estado_adopcion $request, $estado, $id)
				{
					if ($estado == "aprobada")
					{
						if (Auth::user()->id_tipo_usuario == 3)
							{
								DB::table('adopciones')
								->where('id', $id)
								->update(['estado' => $estado]);
								return redirect()->route('administrar_adopciones_admin');
							}
							else
							{
								return redirect()->route('administrar_adopciones');
							}
						}
						else if ($estado == "eliminar")
						{
							$cambios = DB::table('adopciones')
							->where('id_usuario', Auth::user()->id)
							->where('id', $id)
							->delete();

							if(Auth::user()->id_tipo_usuario != 3)
								{
									return redirect()->route('administrar_adopciones');
								}
								else
								{
									return redirect()->route('administrar_adopciones_admin');
								}
							}
							else
							{
								$cambios = DB::table('adopciones')
								->where('id_usuario', Auth::user()->id)
								->where('id', $id)
								->update(['estado' => $estado]);
								return redirect()->route('administrar_adopciones');
							}
						}
					}
